import axios from "axios";

export default {
   async doRequest(params) {
      let response = null;
      try {
         response = await axios(params);
      } catch (error) {
         console.error(error);
      }
      return response;
   }
};